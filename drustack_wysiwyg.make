api = 2
core = 7.x

; Modules
projects[ckeditor][download][tag] = 7.x-1.16
projects[ckeditor][download][type] = git
projects[ckeditor][subdir] = contrib
projects[htmlpurifier][download][branch] = 7.x-1.x
projects[htmlpurifier][download][type] = git
projects[htmlpurifier][subdir] = contrib
projects[image_resize_filter][download][tag] = 7.x-1.14
projects[image_resize_filter][download][type] = git
projects[image_resize_filter][subdir] = contrib
projects[imce][download][tag] = 7.x-1.9
projects[imce][download][type] = git
projects[imce][subdir] = contrib
projects[imce_crop][download][tag] = 7.x-1.1
projects[imce_crop][download][type] = git
projects[imce_crop][subdir] = contrib
projects[imce_mkdir][download][tag] = 7.x-1.0
projects[imce_mkdir][download][type] = git
projects[imce_mkdir][subdir] = contrib
projects[imce_plupload][download][tag] = 7.x-1.2
projects[imce_plupload][download][type] = git
projects[imce_plupload][subdir] = contrib
projects[imce_rename][download][tag] = 7.x-1.3
projects[imce_rename][download][type] = git
projects[imce_rename][subdir] = contrib
projects[imce_tools][download][branch] = 7.x-1.x
projects[imce_tools][download][type] = git
projects[imce_tools][subdir] = contrib
projects[plupload][download][branch] = 7.x-1.x
projects[plupload][download][type] = git
projects[plupload][subdir] = contrib
projects[quickedit][download][branch] = 7.x-1.x
projects[quickedit][download][type] = git
projects[quickedit][subdir] = contrib

; Libraries
libraries[ckeditor][directory_name] = ckeditor
libraries[ckeditor][download][type] = file
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor%20for%20Drupal/edit/ckeditor_4.4.7_edit.zip
libraries[ckeditor][type] = library
libraries[plupload][directory_name] = plupload
libraries[plupload][download][type] = file
libraries[plupload][download][url] = https://github.com/moxiecode/plupload/archive/v1.5.8.zip
libraries[plupload][patch][1903850] = https://drupal.org/files/issues/plupload-1_5_8-rm_examples-1903850-16.patch
libraries[plupload][type] = library
